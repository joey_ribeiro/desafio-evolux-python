# _*_ encoding: utf8 _*_

from flask import Flask, render_template, request, redirect, url_for, flash
from database import db_session
from querystring_parser import parser
from models import User, Hability

app = Flask(__name__)
app.debug = True

## app configure
@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

app.secret_key = 'OpkoIuhYt67FYghi9u8Yij9u8y7t6fyHjnKLM'

##routes
@app.route("/")
def index():
    return render_template('index.html')

@app.route('/quero-me-candidatar')
def quero_me_candidatar():
    habilidades = [
        {'tag': 'html5', 'nome': 'HTML 5'},
        {'tag': 'css3', 'nome': 'CSS 3'},
        {'tag': 'javascript', 'nome': 'Javascript'},
        {'tag': 'python', 'nome': 'Python'},
        {'tag': 'agile', 'nome': 'Agile'}
    ]
    return render_template('form.html', habilidades=habilidades)

@app.route('/salvar-cadastro', methods=['GET', 'POST'])
def salvar_cadastro():  

    if request.method == 'GET':
        return redirect(url_for('quero_me_candidatar'))

    req = parser.parse(request.stream.read(), use_unicode_utf8=True)

    if req['nome'] == '' or req['email'] == '' or req['twitter'] == '':
        flash(u'Todos os campos sao obrigatorios', 'warning')
        return redirect(url_for('quero_me_candidatar'))

    try:
        new_user = User(req['nome'], req['email'], req['twitter'])
        db_session.add(new_user)

        for key in req['habilidade']:
            db_session.add(Hability(key, req['habilidade'][key], new_user))

        db_session.commit()

    except:
        flash(u'Ocorreu um erro ao acessar o banco de dados', 'warning')
        return redirect(url_for('quero_me_candidatar'))

    flash(u'Obrigado por ter demonstrado interesse conosco!', 'success')
    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run()
