from sqlalchemy.orm import relationship, backref
from sqlalchemy import Column, Integer, String, ForeignKey
from database import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    nome = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    twitter = Column(String(20), unique=True)

    habilities = relationship("Hability", backref="users")

    def __init__(self, nome=None, email=None, twitter=None):
        self.nome = nome
        self.email = email
        self.twitter = twitter

    def __repr__(self):
        return '<Usuario %r>' % (self.nome)

class Hability(Base):
    __tablename__ = 'habilities'
    id = Column(Integer, primary_key=True, autoincrement=True)
    nome = Column(String(20), nullable=False)
    nivel = Column(Integer, nullable=False)
    users_id = Column(Integer, ForeignKey('users.id'))

    user = relationship('User', backref=backref('users', lazy='dynamic'))

    def __init__(self, nome, nivel, user):
        self.nome = nome
        self.nivel = nivel
        self.user = user

    def __repr__(self):
        return "<Habilidade('%s', Nivel '%s')>" % (self.nome, self.nivel)