## Evolux evohire
Aplicativo para cadastro de novos talentos feito em Python/Flask e Bootstrap

Notas:
-----
1. O aplicativo possui um patch em uma biblioteca; deve ser aplicado antes de funcionar. Fiz um Pull request para o a    utor original com o Patch (https://github.com/bernii/querystring-parser/pull/10)

2. Foi utilizado o servidor nginx com uma configuração que permitisse o acesso à pasta com os arquivos estáticos; par    a tal funcionar legal carece dar uma editada em nginx.conf

3. Como não pensei em um ambiente de deploy, inicio manualmente os dois serviços (nginx e o flask); Há também o pacot    e pra usá-lo com o gunicorn.

4. Quaisquer dúvidas acerca do sistema basta entrar em contato! <joepreludian@gmail.com>

5. Para instalar as dependências visuais, basta rodar o comando de instalação de pacotes do bower
    
	 cd static/
    bower install

